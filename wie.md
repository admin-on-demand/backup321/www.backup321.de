---
title: wie
tags: [Mermaid]
mermaid: true
---
# Wie

Die erste Grafik zeigt, wie das Backup der nextcloud-Daten funktionieren soll:

<div class="mermaid">
graph LR

subgraph nx9666
  nc_wirsing(wirsing)
end

subgraph backup01
  bu_wirsing(wirsing)
end

subgraph server3000
  s_wirsing(wirsing)
end

subgraph storageshare
  ss_wirsing(wirsing)
end

nc_wirsing --nextcloud_cmd--> s_wirsing
s_wirsing  --rsync--> bu_wirsing
s_wirsing  --rsync--> ss_wirsing
</div>

<div class="mermaid">
graph TD

subgraph nx9666
  nc_jobnummern(jobnummern)
  nc_keepass(keepass)
  nc_kreation(kreation)
  nc_office(office)
end

subgraph backup01
  bu_jobnummern(jobnummern)
  bu_keepass(keepass)
  bu_kreation(kreation)
  bu_office(office)
end

subgraph server3000
  s_jobnummern(jobnummern)
  s_keepass(keepass)
  s_kreation(kreation)
  s_office(office)
end

subgraph storageshare
  ss_archiv(archiv)
  ss_jobnummern(jobnummern)
  ss_keepass(keepass)
  ss_kreation(kreation)
  ss_office(office)
end

s_jobnummern  --rsync--> bu_jobnummern
s_keepass     --rsync--> bu_keepass
s_kreation    --rsync--> bu_kreation
s_office      --rsync--> bu_office

nc_jobnummern --nextcloud_cmd--> s_jobnummern --rsync--> ss_jobnummern
nc_keepass    --nextcloud_cmd--> s_keepass    --rsync--> ss_keepass
nc_kreation   --nextcloud_cmd--> s_kreation   --rsync--> ss_kreation
nc_office     --nextcloud_cmd--> s_office     --rsync--> ss_office

</div>

Die erste Grafik zeigt, wie das Backup der nextcloud-Daten funktionieren soll:

<div class="mermaid">
graph TD

subgraph nx9666
  nc_jobnummern(jobnummern)
  nc_keepass(keepass)
  nc_kreation(kreation)
  nc_office(office)
end

subgraph backup01
  bu_archiv(archiv)
  bu_fonts(fonts)
  bu_jobnummern(jobnummern)
  bu_keepass(keepass)
  bu_kreation(kreation)
  bu_office(office)
end

subgraph server3000
  s_archiv(archiv)
  s_fonts(fonts)
  s_flohmarkt(flohmarkt)
  subgraph s_nextcloud[nextcloud]
    s_jobnummern(jobnummern)
    s_keepass(keepass)
    s_kreation(kreation)
    s_office(office)
  end
end

subgraph storageshare
  ss_archiv(archiv)
  ss_jobnummern(jobnummern)
  ss_keepass(keepass)
  ss_kreation(kreation)
  ss_office(office)
end

s_archiv --rsync--> bu_archiv
s_fonts --rsync--> bu_fonts
s_jobnummern  --rsync--> bu_jobnummern
s_keepass     --rsync--> bu_keepass

s_archiv --rsync--> ss_archiv

nc_jobnummern  --nextcloud_cmd--> s_jobnummern  --rsync--> ss_jobnummern
nc_keepass  --nextcloud_cmd--> s_keepass     --rsync--> ss_keepass
nc_kreation --nextcloud_cmd--> s_kreation    --rsync--> ss_kreation
nc_office   --nextcloud_cmd--> s_office      --rsync--> ss_office

</div>
