---
title: Kein Backup? Kein Mitleid.
---

### Chantal
hat sich ein neues Laptop gekauft. Das alte macht nur noch Zicken. Es bootet nicht mehr richtig, aber es macht komische Geräusche. Die Datenübernahme von altem nach neuem Laptop scheitert. Jetzt liegt das kaputte Laptop unten im Kleiderschrank, darauf sind ja noch die Babyfotos!

### Gerold
hat sich eine USB-Festplatte gekauft. Dann die Backup-Software seines Betriebssystems so eingerichtet, dass es diese Platte als Medium verwendet. Nach dem Backup kommt die Platte in die Schublade. Das hat ein Jahr lang gut geklappt, aber das letzte Backup ist jetzt schon ein halbes Jahr her. Denn das USB-Kabel ist irgendwie verschwunden. Gerold muss mal ein neues kaufen, ehrlich!

### Die Rechtsanwaltskanzlei
hat den Server unter dem Schreibtisch am Empfang stehen, sonst ist nirgendwo Platz. Für das Backup ist am Server eine USB-Platte angesteckt. Diese ist so heiß, dass ich sie nicht anfassen kann. Niemand weiss, wie man an die Daten herankäme, selbst wenn die Platte noch funktionieren würde. Deswegen sind alle dazu übergegangen, die Prozessunterlagen per Dropbox an die Mitarbeiter zu verteilen. Die kaputte Datei, die ich wiederherstellen soll, bleibt kaputt.

### Ernesto
hat eigentlich alles richtig gemacht. Hat jede Woche die Backup-Platte am Server ausgetauscht und die andere wieder mit nach Hause genommen. Dann kommt der Ernstfall und er muss feststellen, dass auf der einzigen ihm verbliebenen Festplatte NICHT alle Daten vorhanden sind, die für den Betrieb der Firma notwendig sind.

### Jan
hat jahrelang brav jeden Tag das DAT-Band (ja, lange her) an seinem Computer gewechselt. Als die Festplatte kaputtgeht, können wir von den Bändern NICHTS wiederherstellen.

### Mafalda
speichert alles in der Cloud, es ist ja nichts vertrauliches dabei. Irgendwann bemerkt sie, dass sie die Excel-Tabelle mit den Passwörtern (ehrlich Mafalda, mach das nicht) in einer älteren Version braucht, weil Daten fehlen. Aber Die Wiederherstellung der Cloud-Daten kann nur ALLES AUF EINMAL wiederherstellen. Damit würde sie auch alle anderen Änderungen seitdem rückgängig machen. Was nun?

Das sind Geschichten, die ich so oder sehr ähnlich in 20 Jahren als admin-on-demand selbst miterlebt habe. Auch doppelt oder dreifach. Es ist bitter, wenn jemand sich schon Mühe gegeben hat und im Ernstfall sind dann doch die wichtigen Daten futsch. Backups **richtig** machen ist nicht ohne.

# 321
Eine bewährte Strategie für gute Backups heißt 321. Von allen Daten sollen mindestens
- drei Kopien existieren, auf mindestens
- zwei verschiedenen Datenträgern, davon
- eine außer Haus

### 3
Drei Kopien. Was in diesem Fall nicht zählt, sind gespiegelte Festplatten oder RAIDs. Denn wenn man dort eine Datei löscht oder kaputtfummelt, ist auch die gespiegelte "Kopie" futsch. Nein, es müssen drei richtige Kopien sein.

### 2
Ja, bei dieser Strategie dürfen zwei Kopien auf ein und dem selben Datenträger liegen. Muss man aber nicht machen. Man darf auch drei Datenträger verwenden.

### 1
Ein Backup außer Haus. Falls die Hütte gebrannt hat oder eingebrochen wurde oder Wassereinbruch im Serverkeller oder oder oder

# Noch ein Satz Anforderungen für gute Backups

[Schlaue Menschen definieren](https://youtu.be/ibk3goTcdHE?t=1142) ein ideales Backup als
- unveränderbar
- unabhängig
- isoliert
- versioniert
- verifiziert
- überwacht
- Risiko-basiert

### Unveränderbar
Den Zugriff von Außen auf die gesicherten Daten darf das Backup **nur lesend** erlauben.
Ein wildgewordener Verschlüsselungstrojaner darf nicht schreibend auf ein vorhandenes Backup zugreifen können.
Ein verärgerter Mitarbeiter darf vorhandene Backups nicht löschen können.

### Unabhängig
Die Backup-Maschine soll komplett eigenständig laufen können und keine externen Abhängigkeiten haben. Kein externer Festspeicher (NAS) als einziges Speichermedium!

### Isoliert
Die Zugriffsrechte auf das Backup dürfen nicht von externen Diensten (ActiveDirectory) abhängen. Ein kompromitiertes AD darf nicht unberechtigten (schreibenden) Zugriff auf das Backup ermöglichen. Das Backup muss die Zugriffsrechte selbst verwalten.

### Versioniert
Das Backup muss mehrere Versionen der Originaldaten vorhalten. Manchmal muss man einen älteren Stand einer Datei wiederherstellen, weil man eine Beschädigung nicht schnell genug bemerkt hat.

### Verifiziert
Lassen sich die Daten aus dem Backup im Ernstfall **wirklich** wiederherstellen? Das Backup soll dafür periodisch einen Test durchführen.

### Überwacht
Zu jeder Zeit muss man nachsehen können, ob das Backup funktioniert. Wann es zuletzt gelaufen ist. Wie viele Daten gesichert wurden. Welche Fehler dabei aufgetreten sind. Ist die Verifizierung erfolgreich? Mit Warnungen können und müssen wir ja leben, mit Fehlern aber nicht.

### Risiko-basiert
Im Ernstfall muss es möglich sein, geschäftskritische Daten bevorzugt wiederherzustellen. Wenn der Wiederherstellungsvorgang Tage oder Wochen dauert und einzelne Daten nicht priorisiert werden können (Tape), dann entsteht weiterer Schaden durch Handlungsunfähigkeit **obwohl** das Backup funktioniert hat.

# Aber Oliver, diese beiden Strategien sind unvereinbar!
Wie soll man unter einen Hut bringen, dass eine Kopie außer Haus gehört, aber gleichzeitig keine externen Abhängigkeiten erlauben? Geht nicht. Müssen wir einen Kompromiss machen.
